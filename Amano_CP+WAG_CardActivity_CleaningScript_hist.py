import pandas as pd
import datetime
import numpy as np

#for historical data loads
xls = pd.ExcelFile('CP+WAG_CardActivityReport_2018.xls') #insert your input file path
df1 = pd.read_excel(xls, 'Sheet1', header = 9)
df2 = pd.read_excel(xls, 'Sheet2')
df3 = pd.read_excel(xls, 'Sheet3')
df4 = pd.read_excel(xls, 'Sheet4')
df5 = pd.read_excel(xls, 'Sheet5')
df6 = pd.read_excel(xls, 'Sheet6')
df7 = pd.read_excel(xls, 'Sheet7')
df8 = pd.read_excel(xls, 'Sheet8')


df1 = df1.dropna(axis = 1, how = "all")
df1 = df1.dropna()
df1.rename(columns={'Unnamed: 2':'card_number','Date and Time':'logged_date','Reader ':'reader','Lot ':'lot','Direction':'direction','Result':'result','Allowed':'allowed'}, inplace=True)

df2 = df2.dropna(axis = 1, how = "all")
df2 = df2.dropna()

df3 = df3.dropna(axis = 1, how = "all")
df3 = df3.dropna()

df4 = df4.dropna(axis = 1, how = "all")
df4 = df4.dropna()

df5 = df5.dropna(axis = 1, how = "all")
df5 = df5.dropna()

df6 = df6.dropna(axis = 1, how = "all")
df6 = df6.dropna()

df7 = df7.dropna(axis = 1, how = "all")
df7 = df7.dropna()

df8 = df8.dropna(axis = 1, how = "all")
df8 = df8.dropna()

df2.columns = ['card_number','logged_date','reader','lot','direction','result','allowed']
df3.columns = ['card_number','logged_date','reader','lot','direction','result','allowed']
df4.columns = ['card_number','logged_date','reader','lot','direction','result','allowed']
df5.columns = ['card_number','logged_date','reader','lot','direction','result','allowed']
df6.columns = ['card_number','logged_date','reader','lot','direction','result','allowed']
df7.columns = ['card_number','logged_date','reader','lot','direction','result','allowed']
df8.columns = ['card_number','logged_date','reader','lot','direction','result','allowed']


#merging all data into one
data = pd.concat([df1, df2,df3, df4,df5,df6,df7,df8], ignore_index=True)

data = data.sort_values('logged_date')
data.insert(0, "id", np.nan, True)
data.insert(8, "garage_id", 1, True)
data['logged_date'] = pd.to_datetime(data['logged_date'])
data = data.reset_index(drop = True)
data.to_csv("Clean_CP+WAG_CardActivityReport_2018.csv", index = False) #file naming convention: Clean_Garagename_Reportname_YY.csv

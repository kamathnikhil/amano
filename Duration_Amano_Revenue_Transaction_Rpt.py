import pandas as pd
import numpy as np
import datetime
from datetime import timedelta

data = pd.read_csv('Clean_Amano_Revenue_Transaction_Rpt_YY.csv') #raw input file

def timeDiff(new_df):
    edf = pd.DataFrame(columns=['ID','GROSS_AMOUNT','NET_AMOUNT','AT_ID','RATE','TICKET_TYPE','TICKET_NUMBER','ENTRY_TIME','EXIT_TIME','DURATION','TRANS_NUMBER','DEVICE_NAME','GARAGE_ID'])
    new_df['ENTRY_TIME'] = pd.to_datetime(new_df['ENTRY_TIME'])
    new_df['EXIT_TIME'] = pd.to_datetime(new_df['EXIT_TIME'])        
    DURATION = []
    for j in range(new_df.shape[0]):
            GROSS_AMOUNT = new_df['GROSS_AMOUNT'].iloc[j]
            ENTRY_TIME = new_df['ENTRY_TIME'].iloc[j]
            EXIT_TIME = new_df['EXIT_TIME'].iloc[j]
            dateTimeDifference = pd.to_timedelta(new_df['EXIT_TIME'].iloc[j] - new_df['ENTRY_TIME'].iloc[j])/3600 
            DURATION = dateTimeDifference.total_seconds()
            NET_AMOUNT = new_df.NET_AMOUNT.iloc[j]
            AT_ID = new_df.AT_ID.iloc[j]
            RATE = new_df.RATE.iloc[j]
            GARAGE_ID = '1'
            TRANS_NUMBER = new_df['TRANS_NUMBER'].iloc[j]
            TICKET_NUMBER = new_df['TICKET_NUMBER'].iloc[j]
            TICKET_TYPE = new_df['TICKET_TYPE'].iloc[j]
            DEVICE_NAME = new_df['DEVICE_NAME'].iloc[j]
            edf = edf.append({'GROSS_AMOUNT':GROSS_AMOUNT,'NET_AMOUNT': NET_AMOUNT,'AT_ID': AT_ID,'RATE':RATE,'TICKET_TYPE':TICKET_TYPE,'TICKET_NUMBER':TICKET_NUMBER,'ENTRY_TIME':ENTRY_TIME,'EXIT_TIME': EXIT_TIME,'DURATION': DURATION,'TRANS_NUMBER':TRANS_NUMBER,'DEVICE_NAME':DEVICE_NAME,'GARAGE_ID':GARAGE_ID},ignore_index=True)
    edf = edf.sort_values('ENTRY_TIME')
#mention your output file path here. From here you will get your desired csv files from this function
    outpath = r'D:\Card_Activity'
    filename = "Duration_Amano_Revenue_Transaction_Rpt_"+(edf['ENTRY_TIME'].max().strftime("%m%d%Y_%H%M"))+".csv" #file naming convention Duration_Amano_Reportname_mmddyy_hhmm.csv
    edf.to_csv(outpath + "\\" + filename, index = False)
    return edf
    
op = timeDiff(data)
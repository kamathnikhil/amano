import pandas as pd
import datetime
import numpy as np

df = pd.read_excel('Amano_RevenueTransactionReport_MM_DD_YY.xls') #raw input file

def cleaning(df):
    dfrow = df.iloc[np.flatnonzero((df=='Gross').values)//df.shape[1],:]
    idx=dfrow.index+2
    df2 = df[idx[0]+1:]
    df2['Unnamed: 32'] = df2['Unnamed: 32'].fillna(df2['Unnamed: 31'])
    df2['Unnamed: 35'] = df2['Unnamed: 35'].fillna(df2['Unnamed: 33'])
    df2['Unnamed: 38'] = df2['Unnamed: 38'].fillna(df2['Unnamed: 37'])
    df3 = df2.dropna(axis =1, how = "all")
    df3 = df3.dropna(axis =0, how = "all")
    df3 = df3.drop(['Unnamed: 31','Unnamed: 33','Unnamed: 37','Unnamed: 16'], axis = 1)
    df3.rename(columns={'Unnamed: 2':'GROSS_AMOUNT','Unnamed: 7':'NET_AMOUNT','Unnamed: 11':'AT_ID','Unnamed: 13':'RATE','Unnamed: 19':'TRANSACTION_TYPE','Unnamed: 22':'TICKET_NUMBER','Unnamed: 27':'ENTRY_TIME','Unnamed: 32':'EXIT_TIME','Unnamed: 35':'TRANS_NUMBER','Unnamed: 38':'DEVICE_NAME'}, inplace=True)
    df3['ENTRY_TIME'] = pd.to_datetime(df3['ENTRY_TIME'])
    df3['EXIT_TIME'] = pd.to_datetime(df3['EXIT_TIME'])
    df3 = df3.sort_values('ENTRY_TIME')
    df3 = df3.reset_index(drop = True)
    df3.insert(0, "ID", np.nan, True)
    #mention your output file path here. From here you will get your desired csv files from this function
    outpath = r'D:\Card_Activity'
    filename = "Clean_Amano_Revenue_Transaction_Rpt_"+(df3['ENTRY_TIME'].max().strftime("%m%d%Y_%H%M"))+".csv" #file naming convention Clean_Amano_Reportname_YY.csv
    df3.to_csv(outpath + "\\" + filename, index = False)
    return df3

cleaning(df)
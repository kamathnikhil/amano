import pandas as pd
from datetime import datetime
import numpy as np

df = pd.read_excel('Card_Activity_User_1072019.xls') #insert your raw input file path here Card_Activity_User_mmddyy.xls

def cleaning(df):
    dfnew = df.iloc[np.flatnonzero((df=='Card Number').values)//df.shape[1],:]
    idx=dfnew.index
    dfnew = df[idx[0]+1:]
#     dfrow = dfnew.iloc[np.flatnonzero((dfnew=='Number of cards in report: 706').values)//dfnew.shape[1],:]
#     idx2=dfrow.index
#     n = len(dfnew)-idx2[0]
    dfnew = dfnew[:-6]
    dfnew = dfnew.dropna(axis = 1,how = "all")
    dfnew = dfnew.dropna(axis = 0,how = "all")
    dfnew.rename(columns={'Unnamed: 1':'Card_Number','Unnamed: 3':'Account_Number','Unnamed: 6':'Access_Groups','Unnamed: 8':'LastName','Unnamed: 14':'FirstName','Unnamed: 17':'Status','Unnamed: 20':'Mode'}, inplace=True)
    dfnew = dfnew.drop(['Mode','Status'], axis = 1)
    dfnew = dfnew.where((pd.notnull(dfnew)), "None")
    dfnew.insert(0, "ID", np.nan, True)
    dfnew = dfnew.reset_index(drop = True)
    outpath = r'D:\Card_Activity'
    filename = "Clean_Amano_Card_Activity_User_Rpt_"+(datetime.now().strftime("%m%d%Y_%H%M"))+".csv"  #file naming convention Clean_Amano_Reportname_mmddYY_HHMM.csv
    dfnew.to_csv(outpath + "\\" + filename, index = False)
    return dfnew
    
cleaning(df)
import pandas as pd
import datetime as dt
import numpy as np

df = pd.read_excel('WAG_CountActivityReport_YY.xls') #insert raw input file path here

def cleaning(df):
    dfnew = df.iloc[np.flatnonzero((df=='Date and Time').values)//df.shape[1],:]
    idx=dfnew.index
    dfnew = df[idx[0]+1:]    
    dfnew = dfnew.dropna(axis = 1, how = "all")
    dfnew.rename(columns={'Unnamed: 1':'Datetime','Unnamed: 3':'Contract','Unnamed: 5':'Facility','Unnamed: 8':'Transient'}, inplace=True)
    dfnew = dfnew.dropna()
    dfnew = dfnew.sort_values('Datetime')
    dfnew.insert(0, "id", np.nan, True)
    dfnew['Datetime'] = pd.to_datetime(dfnew['Datetime'])
    dfnew = dfnew.reset_index(drop = True)
    outpath = r'D:\Card_Activity'
    filename = "Clean_WAG_CountActivityReport_"+(dfnew['Datetime'].max().strftime("%m%d%Y_%H%M"))+".csv" #file naming convention Clean_WAG_Reportname_mmddYY_HHMM.csv
    dfnew.to_csv(outpath + "\\" + filename, index = False)
    return dfnew

test = cleaning(df)

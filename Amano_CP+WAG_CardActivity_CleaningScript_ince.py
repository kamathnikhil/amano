import pandas as pd
import numpy as np
import datetime
from datetime import timedelta
from datetime import datetime
import time

df = pd.read_excel("CP+WAG_CardActivityReport_MM_DD_YY.xls") # insert your raw input file path 

#for incremental data loads
def cleaning(df):
    dfrow = df.iloc[np.flatnonzero((df=='Date and Time').values)//df.shape[1],:]
    idx=dfrow.index
    dfnew = df[idx[0]+1:]
    dfnew.rename(columns={'Unnamed: 2':'card_number','Unnamed: 4':'logged_date','Unnamed: 8':'reader','Unnamed: 12':'lot','Unnamed: 14':'direction','Unnamed: 16':'result','Unnamed: 20':'allowed'}, inplace=True)
    dfnew = dfnew.dropna(axis = 1, how = "all")
    dfnew = dfnew.dropna()
    dfnew = dfnew.sort_values('logged_date')
    dfnew['logged_date'] = pd.to_datetime(dfnew['logged_date'])
    dfnew = dfnew.reset_index(drop = True)
    dfnew.insert(0, "id", np.nan, True)
    dfnew.insert(8, "garage_id", 1, True)
    #mention your output file path here. From here you will get your desired csv files from this function
    outpath = r'D:\Card_Activity'
    filename = "Clean_Amano_CP+WAG_Card_Activity_Rpt_"+(dfnew['logged_date'].max().strftime("%m%d%Y_%H%M"))+".csv" # file naming convention Clean_Amano_Garagename_Reportname_mmddYY_HHMM.csv
    dfnew.to_csv(outpath + "\\" + filename, index = False)
    return dfnew

test = cleaning(df)

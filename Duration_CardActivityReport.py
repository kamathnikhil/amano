import pandas as pd
import numpy as np
import datetime
from datetime import timedelta

data = pd.read_csv('Clean_CP+WAG_CardActivityReport_YY.csv') #rawinput filename path

#created a dynamic function that creates a dataframe with required data for further analysis
def timeDiff(df):
    l = []
    edf = pd.DataFrame(columns=['id','card_number', 'logged_date','duration','reader','lot','direction','result','allowed','garage_id'])
    l = df['card_number'].unique()
    for i in l:
        new_df = df[df['card_number']==i]
        new_df = new_df.reset_index(drop=True)
        new_df['logged_date'] = pd.to_datetime(new_df['logged_date'])
        #print("card_number N is : {} and df size is : {}".format(i,new_df.shape[0]))
        for j in range(new_df.shape[0]):
            #print("J value is : {} and j+1 is : {}".format(j,j+1))
            if(j<new_df.shape[0]-1):
                card_number = new_df.card_number.iloc[j]
                logged_date = new_df.logged_date.iloc[j]
                Difference = pd.to_timedelta(new_df.logged_date.iloc[j+1] - new_df.logged_date.iloc[j])/3600 
                duration = Difference.total_seconds()
                reader = new_df.reader.iloc[j]
                direction = new_df.direction.iloc[j]
                lot = new_df.lot.iloc[j]
                id = new_df.id.iloc[j]
                garage_id = new_df.garage_id.iloc[j]
                result = new_df.result.iloc[j]
                allowed = new_df.allowed.iloc[j]
                edf = edf.append({'id':id,'card_number':card_number, 'logged_date':logged_date, 'duration':duration,'reader': reader,'lot':lot,'direction':direction,'result':result,'allowed':allowed,'garage_id':garage_id},ignore_index=True)
            else: 
                card_number = new_df.card_number.iloc[j]
                logged_date = new_df.logged_date.iloc[j]
                duration = 0
                reader = new_df.reader.iloc[j]
                direction = new_df.direction.iloc[j]
                lot = new_df.lot.iloc[j]
                id = new_df.id.iloc[j]
                garage_id = new_df.garage_id.iloc[j]
                result = new_df.result.iloc[j]
                allowed = new_df.allowed.iloc[j]
                edf = edf.append({'id':id,'card_number':card_number, 'logged_date':logged_date,'duration':duration,'reader': reader,'lot':lot, 'direction':direction,'result':result,'allowed':allowed,'garage_id':garage_id},ignore_index=True)   
    edf = edf.sort_values('logged_date')
    edf = edf.reset_index(drop=True)
#mention your output file path here. From here you will get your desired csv files from this function
    outpath = r'D:\Card_Activity'
    filename = "Duration_Amano_Card_Activity_Rpt_"+(edf['logged_date'].max().strftime("%m%d%Y_%H%M"))+".csv" #file naming convention Duration_Amano_reportname_mmddyy_hhmm.csv
    edf.to_csv(outpath + "\\" + filename, index = False)
    return edf

final = timeDiff(data)

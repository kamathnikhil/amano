import pandas as pd
import datetime
import numpy as np


xls = pd.ExcelFile('Amano_RevenueTransactionReport_Jan-Oct_2019.xls') #raw input file path
df1 = pd.read_excel(xls, 'Sheet1', header = 13)
df2 = pd.read_excel(xls, 'Sheet2')

df1['Unnamed: 32'] = df1['Unnamed: 32'].fillna(df1['Date and Time.1'])
df1['Unnamed: 35'] = df1['Unnamed: 35'].fillna(df1['Unnamed: 33'])
df1['Unnamed: 38'] = df1['Unnamed: 38'].fillna(df1['Device Name'])
df1 = df1.drop(['Date and Time.1','Unnamed: 33','Device Name','Type'], axis = 1)
df1 = df1.dropna(axis =0, how = "all")
df1 = df1.dropna(axis =1, how = "all")
df1.rename(columns={'Amount':'GROSS_AMOUNT','Unnamed: 7':'NET_AMOUNT','ID':'AT_ID','Rate':'RATE','Unnamed: 19':'TICKET_TYPE','Number':'TICKET_NUMBER','Unnamed: 27':'ENTRY_TIME','Unnamed: 32':'EXIT_TIME','Unnamed: 35':'TRANS_NUMBER','Unnamed: 38':'DEVICE_NAME'}, inplace=True)
df1 = df1.reset_index(drop = True)
df1.insert(0, "ID", np.nan, True)

dfrow = df2.iloc[np.flatnonzero((df2=='Colors:').values)//df2.shape[1],:]
idx=dfrow.index
n = len(df2)-idx[0]
df3 = df2[:-n]
df3 = df3.dropna(axis =1, how = "all")
df3 = df3.dropna(axis =0, how = "all")
df3 = df3.reset_index(drop = True)
df3.columns=['GROSS_AMOUNT','NET_AMOUNT','AT_ID','RATE','Entry','TICKET_TYPE', 'TICKET_NUMBER','ENTRY_TIME','Unnamed: 31','EXIT_TIME','Unnamed: 33','TRANS_NUMBER','Unnamed: 37','DEVICE_NAME']
df3['Unnamed: 31'] = df3['Unnamed: 31'].fillna(df3['EXIT_TIME'])
df3['TRANS_NUMBER'] = df3['TRANS_NUMBER'].fillna(df3['Unnamed: 33'])
df3['DEVICE_NAME'] = df3['DEVICE_NAME'].fillna(df3['Unnamed: 37'])
df3 = df3.drop(['Entry','EXIT_TIME','Unnamed: 33','Unnamed: 37'], axis = 1)
df3 = df3.rename(columns = {'Unnamed: 31':'EXIT_TIME'})
df3.insert(0, "ID", np.nan, True)

data = pd.concat([df1, df3], ignore_index=True)

data['ENTRY_TIME'] = pd.to_datetime(data['ENTRY_TIME'])
df3['EXIT_TIME'] = pd.to_datetime(data['EXIT_TIME'])
data = data.sort_values('ENTRY_TIME')
data = data.reset_index(drop = True)

data.to_csv("Clean_Amano_Revenue_Transaction_Rpt_2019.csv",index = False) #file naming convention Clean_Amano_Reportname_yy.csv